# Map Based Decision Support System
## Purpose
The application is intended to plan deployment of acoustic ranging unit according to its predefined scouting area.
## Allied Matters
1) calibration the map in order to determine parameters of transformation from screen coordinates to map coordinates and backward;
2) determining map coordinates (“grids” in terms of military) of the point that user chooses on a loaded map;
3) drawing right and left arrow-ended lines that bound the scouting area;
4) computing the grids of acoustic bases (“base points”) and its directions according to the scouting area and drawing it on a loaded map with proper symbols;
5) manual correction of acoustic bases positions;
6) computing the grids of the reconnaissance zone according to corrected positions of acoustic bases and drawing its bounds on a loaded map;
7) choosing arbitrary point inside the reconnaissance zone by mouse double click and sending its grids to the server (application that simulates functioning of devices of acoustic ranging complex) through the socket connection.
## Testing the application
1) load source files and build the application in MS Visual Studio 2017;
2) click button “Map Load” and choose the in OpenFileDialog the appropriate file (*.jpg, *.bmp etc) with topographic map image;
3) point with mouse click three grid lines intersections away from each other, and enter its grids in appropriate textboxes; i.e. if point #1 is an intersection of horizontal line 80 and vertical live 82, user have to enter “80000” in “X” textbox and “82000” in “Y” textbox for coordination point #1.
3) click button “Calibrate map”; transformation parameters (rotation angle and scale) will be computed and showed on status bar. Now when you click on a map, point’s grids will appear on status bar. If you double click on a map, point’s grids will appear in text boxes X Y to be sent through the socket connection further.
4) continuously choose points of scouting area with right click and defining them in context menu.
5) click the button «Визначити положення акустичних баз» (“Define the acoustic bases positions”); the symbols of acoustic bases will appear on the map in proper places, and its coordinates and grid angles will appear in textboxes; reconnaissance zone boundaries will be drawn too.
6) you can correct the positions with mouse right click in new location; reconnaissance zone will be redrawn automatically/
7) now you can choose target within reconnaissance zone, double-click it and send its grids through socket connection by clicking “Send it” button.
