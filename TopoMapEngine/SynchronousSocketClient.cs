﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace TopoMapEngine
{
   public class SynchronousSocketClient
    {
        public static void StartClient(string StrToSend)
        {
            // Data buffer for incoming data.  
            byte[] bytes = new byte[1024];

            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // This example uses port 11000 on the local computer.  
               
                // IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                IPHostEntry ipHostInfo = Dns.GetHostEntry(Form1.SrvHostName);
                IPAddress ipAddress = ipHostInfo.AddressList[0];
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11000);

                // Create a TCP/IP  socket.  
                Socket sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    sender.Connect(remoteEP);

                    Form1.msg = "Socket connected to " + sender.RemoteEndPoint.ToString();

                    // Encode the data string into a byte array.  
                    byte[] msg = Encoding.ASCII.GetBytes(StrToSend);

                    // Send the data through the socket.  
                    int bytesSent = sender.Send(msg);

                    // Receive the response from the remote device.  
                    int bytesRec = sender.Receive(bytes);
                    Form1.msg = "Echoed test = " + Encoding.ASCII.GetString(bytes, 0, bytesRec);

                    // Release the socket.  
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();

                }
                catch (ArgumentNullException ane)
                {
                    Form1.msg = "ArgumentNullException : " + ane.ToString();
                }
                catch (SocketException se)
                {
                    Form1.msg = "SocketException : " + se.ToString();
                }
                catch (Exception e)
                {
                    Form1.msg = "Unexpected exception : " + e.ToString();
                }

            }
            catch (Exception e)
            {
                Form1.msg = e.ToString();
            }
        }

      
    }

    }
