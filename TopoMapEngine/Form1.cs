﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace TopoMapEngine
{
    public partial class Form1 : Form
    {
        static double MSR; // Map-to-Screen Ratio
        static double RA; // coord system rotation angle- from screen to map
        static double X0_nod; static double Y0_nod;
        static double Xconv_nod; static double Yconv_nod;
        static bool MapIsLoaded = false;
        static bool MapIsCalibrated = false;
        static bool BP_IsDefined = false;
        static int RightClickX = 0;
        int RightClickY = 0;
        int BP_radius = 12;
        string mapfileName = "";
        public static string msg;
        public static string TxtMsgToSend = "911111_922222<EOF>";  // test message
        public class MapPoint
        {
            public double X_map, Y_map, X_scr, Y_scr;

            public MapPoint(double X, double Y)
            {
                this.X_scr = X; this.X_map = X;
                this.Y_scr = Y; this.Y_map = Y;
            }

            public void ConvertScrToMap()
            {
               
                Transforms.DoCrdConvert(out X_map, out Y_map,
                                 X0_nod, Y0_nod, -RA,   // 
                                 Xconv_nod, Yconv_nod,
                                 X_scr * MSR, Y_scr * MSR);
            }

            public void ConvertMapToScr()
            {

                Transforms.DoCrdConvert(out X_scr, out Y_scr,
                                Xconv_nod, Yconv_nod, RA,   // 
                                X0_nod, Y0_nod,
                                X_map, Y_map);
                X_scr /= MSR; Y_scr /= MSR;
            }
        }   // MapPoint


        public class CPoint : MapPoint
        {
            static public int CalibrationPointNr = 0;
            public CPoint(double X, double Y) : base(X, Y)
            {
                if (CalibrationPointNr < 3) CalibrationPointNr++;
                else CalibrationPointNr = 0;
            }
        } // CPoint


        MapPoint MidRearRecceAreaPt = new MapPoint(0, 0); // Middle line of scouting area: rear point
        MapPoint MidFrontRecceAreaPt = new MapPoint(0, 0); //                              front point
        MapPoint TPD_Pt = new MapPoint(0, 0);              // Point of leading lines of Base Points

        public static string SrvHostName = "WIN-2JF1S2VBSQ5";



        public Form1()
        {
            InitializeComponent();
        }

        private void btnMapLoad_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pictureBox1.Load(openFileDialog1.FileName);
                MapIsLoaded = true;
                toolStripStatusLabel1.Text = "Карту завантажено";
                mapfileName = openFileDialog1.FileName;

                // pictureBox1.Load(mapfileName);
            }
        }  // btnMapLoad_Click


        CPoint[] pt = new CPoint[5];
        


        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            switch (e.Button)
            {
                case MouseButtons.Right:
                    {
                        contextMenuStrip1.Show(this, new Point(e.X, e.Y)); //places the menu at the pointer position
                        RightClickX = e.X;
                        RightClickY = e.Y;
                    }
                    break;
            }
        }



        int X1;
        int Y1;

        public void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            X1 = e.X; Y1 = e.Y; label1.Text = X1.ToString(); label2.Text = Y1.ToString();

            if (!MapIsCalibrated)
            {
                pt[CPoint.CalibrationPointNr + 1] = new CPoint(e.X, e.Y);

                lblCPointNr.Text = CPoint.CalibrationPointNr.ToString();

                toolStripStatusLabel1.Text = "Вибрано точку №" + CPoint.CalibrationPointNr.ToString();

                Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
                System.Drawing.SolidBrush brush1 = new System.Drawing.SolidBrush(System.Drawing.Color.DarkRed);
                g.FillEllipse(brush1, X1 - 4, Y1 - 4, 8, 8);

                Font drawFont = new Font("Arial", 10);
                g.DrawString(CPoint.CalibrationPointNr.ToString(), drawFont, brush1, X1 + 4, Y1 + 4);

                brush1.Dispose(); g.Dispose();
            }
            else
            {
                rpt = new CPoint(e.X, e.Y);
                rpt.ConvertScrToMap();
                toolStripStatusLabel1.Text = "X_map = " + rpt.X_map.ToString("G0") +
                                           ", Y_map = " + rpt.Y_map.ToString("G0");
                Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
                System.Drawing.SolidBrush brush1 = new System.Drawing.SolidBrush(System.Drawing.Color.DarkGreen);
                g.FillEllipse(brush1, X1 - 4, Y1 - 4, 8, 8);

                brush1.Dispose(); g.Dispose();
            }

        } // pictureBox1_MouseClick

        private void btnCalibrate_Click(object sender, EventArgs e)
        {
            if (MapIsLoaded) {
                bool isConverted = true;
                try
            {
                pt[1].X_map = Convert.ToDouble(txtX1.Text);
                pt[1].Y_map = Convert.ToDouble(txtY1.Text);

                pt[2].X_map = Convert.ToDouble(txtX2.Text);
                pt[2].Y_map = Convert.ToDouble(txtY2.Text);

                pt[3].X_map = Convert.ToDouble(txtX3.Text);
                pt[3].Y_map = Convert.ToDouble(txtY3.Text);
            }

            catch (System.FormatException ex)
            {
                MessageBox.Show("Перевірте введення координат точок калібрування");
                    isConverted = false;
            }

            catch (System.NullReferenceException ex)
            {
                MessageBox.Show("Перевірте вибір трьох точок калібрування");
                    isConverted = false;
                }

            catch (System.Exception ex)
            {
                MessageBox.Show("Невідома помилка на етапі калібрування");
                    isConverted = false;
                }

                if (isConverted) { 
                    Transforms.FindRotationParams(out RA, out MSR,    // rotation angle, map to screen ratio
                    pt[1].X_map, pt[1].Y_map, pt[1].X_scr, pt[1].Y_scr,
                    pt[2].X_map, pt[2].Y_map, pt[2].X_scr, pt[2].Y_scr);
                    if (RA < 0) RA = RA + 2 * Math.PI;

                    Transforms.FindRotationParams(out double tempRA, out double tempMSR,    // rotation angle, map to screen ratio
                                pt[2].X_map, pt[2].Y_map, pt[2].X_scr, pt[2].Y_scr,
                                pt[3].X_map, pt[3].Y_map, pt[3].X_scr, pt[3].Y_scr);
                    if (tempRA < 0) tempRA = tempRA + 2 * Math.PI;

                    RA = (RA + tempRA) / 2;
                    MSR = (MSR + tempMSR) / 2;

                    X0_nod = pt[1].X_scr * MSR; Y0_nod = pt[1].Y_scr * MSR;
                    Xconv_nod = pt[1].X_map; Yconv_nod = pt[1].X_map;

                    if ((Math.Abs(RA - tempRA) < 0.1) & (Math.Abs(MSR - tempMSR) < 0.1))
                    MapIsCalibrated = true;
                    else MessageBox.Show("Перевірте калібрування!");
                    toolStripStatusLabel1.Text = "RA = " + RA.ToString(format: "F5") + ", MSR = " + MSR.ToString(format: "F5");
                }
            }
            else MessageBox.Show("Завантажте карту!");
            
        } // btnCalibrate_Click

        CPoint rpt;
        private void pictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            if (MapIsCalibrated)
            {
                rpt = new CPoint(e.X, e.Y);
                rpt.ConvertScrToMap();
                toolStripStatusLabel1.Text = "X_map = " + rpt.X_map.ToString() + ", Y = " + rpt.Y_map.ToString();
                txtBox_Ytgt.Text = ((int)(rpt.Y_map)).ToString();
                txtBox_Xtgt.Text = ((int)(rpt.X_map)).ToString();
            }
            else toolStripStatusLabel1.Text = "Карту не відкалібровано";
        }


        // Initialization of Base Points and grid angles of its leading lines

        MapPoint BP1_ctr = new MapPoint(0, 0); double BP1_Adir;
        MapPoint BP2_ctr = new MapPoint(0, 0); double BP2_Adir;
        MapPoint BP3_ctr = new MapPoint(0, 0); double BP3_Adir;




        // ****************************************************************************
        // ****    Scouting area points initialization, drawing the Scouting area   ***
        // ****************************************************************************

        MapPoint RightRearRecceAreaPt = new MapPoint(0, 0);
        MapPoint RightFrontRecceAreaPt = new MapPoint(0, 0);
        MapPoint LeftRearRecceAreaPt = new MapPoint(0, 0);
        MapPoint LeftFrontRecceAreaPt = new MapPoint(0, 0);


        private void RightRearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RightRearRecceAreaPt.X_scr = RightClickX;
            RightRearRecceAreaPt.Y_scr = RightClickY;

            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            System.Drawing.SolidBrush brush1 = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            g.FillEllipse(brush1, (float)RightRearRecceAreaPt.X_scr - 3,
                                  (float)RightRearRecceAreaPt.Y_scr - 3, 5, 5);

            Font drawFont = new Font("Arial", 10);
            g.DrawString("Пр.Бл.Т-ка", drawFont, brush1, (float)RightRearRecceAreaPt.X_scr + 3,
                                                         (float)RightRearRecceAreaPt.Y_scr + 3);

            brush1.Dispose(); g.Dispose();

            RightRearRecceAreaPt.ConvertScrToMap();
            toolStripStatusLabel1.Text = "X_map = " + RightRearRecceAreaPt.X_map.ToString(format: "F0") + "," +
                                         " Y = " + RightRearRecceAreaPt.Y_map.ToString(format: "F0");
        } // RightRearToolStripMenuItem_Click

        private void RightFrontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RightFrontRecceAreaPt.X_scr = RightClickX;
            RightFrontRecceAreaPt.Y_scr = RightClickY;

            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            System.Drawing.SolidBrush brush1 = new System.Drawing.SolidBrush(System.Drawing.Color.Red);

            g.FillEllipse(brush1, (float)RightFrontRecceAreaPt.X_scr - 3,
                                  (float)RightFrontRecceAreaPt.Y_scr - 3, 6, 6);

            Font drawFont = new Font("Arial", 10);
            g.DrawString("Пр.Дальн.Т-ка", drawFont, brush1, (float)RightFrontRecceAreaPt.X_scr + 4,
                                                            (float)RightFrontRecceAreaPt.Y_scr + 4);
            DrawRecceAreaLines();
            brush1.Dispose(); g.Dispose();

            RightFrontRecceAreaPt.ConvertScrToMap();
            toolStripStatusLabel1.Text = "X_map = " + RightFrontRecceAreaPt.X_map.ToString(format: "F0") + "," +
                                         " Y = " + RightFrontRecceAreaPt.Y_map.ToString(format: "F0");
        }

        private void LeftRearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LeftRearRecceAreaPt.X_scr = RightClickX;
            LeftRearRecceAreaPt.Y_scr = RightClickY;

            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            System.Drawing.SolidBrush brush1 = new System.Drawing.SolidBrush(System.Drawing.Color.Red);
            g.FillEllipse(brush1, (float)LeftRearRecceAreaPt.X_scr - 3,
                                  (float)LeftRearRecceAreaPt.Y_scr - 3, 6, 6);

            Font drawFont = new Font("Arial", 10);
            g.DrawString("Лів.Бл.Т-ка", drawFont, brush1, (float)LeftRearRecceAreaPt.X_scr + 3,
                                                         (float)LeftRearRecceAreaPt.Y_scr + 3);

            brush1.Dispose(); g.Dispose();

            LeftRearRecceAreaPt.ConvertScrToMap();
            toolStripStatusLabel1.Text = "X_map = " + LeftRearRecceAreaPt.X_map.ToString(format: "F0") + "," +
                                         " Y = " + LeftRearRecceAreaPt.Y_map.ToString(format: "F0");
        }


        private void LeftFrontToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LeftFrontRecceAreaPt.X_scr = RightClickX;
            LeftFrontRecceAreaPt.Y_scr = RightClickY;

            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            System.Drawing.SolidBrush brush1 = new System.Drawing.SolidBrush(System.Drawing.Color.Red);

            g.FillEllipse(brush1, (float)LeftFrontRecceAreaPt.X_scr - 3,
                                  (float)LeftFrontRecceAreaPt.Y_scr - 3, 6, 6);

            Font drawFont = new Font("Arial", 10);
            g.DrawString("Лів.Дальн.Т-ка", drawFont, brush1, (float)LeftFrontRecceAreaPt.X_scr + 4,
                                                            (float)LeftFrontRecceAreaPt.Y_scr + 4);
            DrawRecceAreaLines();
            brush1.Dispose(); g.Dispose();

            LeftFrontRecceAreaPt.ConvertScrToMap();
            toolStripStatusLabel1.Text = "X_map = " + LeftFrontRecceAreaPt.X_map.ToString(format: "F0") + "," +
                                         " Y = " + LeftFrontRecceAreaPt.Y_map.ToString(format: "F0");
        }

        public void DrawRecceAreaLines()
        {
            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            System.Drawing.SolidBrush brush_RAlines = new System.Drawing.SolidBrush(System.Drawing.Color.DarkBlue);
            System.Drawing.Pen pen_RAlines = new Pen(brush_RAlines);
            pen_RAlines.Width = 2;
            pen_RAlines.EndCap = System.Drawing.Drawing2D.LineCap.ArrowAnchor;

            // Draw right line:
            if (RightRearRecceAreaPt.X_scr != 0 && RightFrontRecceAreaPt.X_scr != 0)
                g.DrawLine(pen_RAlines, (float)RightRearRecceAreaPt.X_scr, (float)RightRearRecceAreaPt.Y_scr,
                            (float)RightFrontRecceAreaPt.X_scr, (float)RightFrontRecceAreaPt.Y_scr);

            // Draw left line:
            if (LeftRearRecceAreaPt.X_scr != 0 && LeftFrontRecceAreaPt.X_scr != 0)
                g.DrawLine(pen_RAlines, (float)LeftRearRecceAreaPt.X_scr, (float)LeftRearRecceAreaPt.Y_scr,
                           (float)LeftFrontRecceAreaPt.X_scr, (float)LeftFrontRecceAreaPt.Y_scr);

            brush_RAlines.Dispose(); pen_RAlines.Dispose(); g.Dispose();
        }

        // ****************************************************************************
        // ***********   End of section of Scouting area drawing    ***************
        // ****************************************************************************

        public void DefinePBpositions()
        {
            MidRearRecceAreaPt.X_map = (LeftRearRecceAreaPt.X_map + RightRearRecceAreaPt.X_map) / 2;
            MidRearRecceAreaPt.Y_map = (LeftRearRecceAreaPt.Y_map + RightRearRecceAreaPt.Y_map) / 2;
            MidRearRecceAreaPt.ConvertMapToScr();

            MidFrontRecceAreaPt.X_map = (LeftFrontRecceAreaPt.X_map + RightFrontRecceAreaPt.X_map) / 2;
            MidFrontRecceAreaPt.Y_map = (LeftFrontRecceAreaPt.Y_map + RightFrontRecceAreaPt.Y_map) / 2;
            MidFrontRecceAreaPt.ConvertMapToScr();

            Transforms.OGZ(out double DD2, out double RecceAreaAngle,
                            MidRearRecceAreaPt.X_map, MidFrontRecceAreaPt.X_map,
                            MidRearRecceAreaPt.Y_map, MidFrontRecceAreaPt.Y_map);

            // обчислення координат точки БП-2 та їх переведення в екранну систему координат:
            BP2_ctr.X_map = MidRearRecceAreaPt.X_map - 2500 * Math.Cos(RecceAreaAngle);
            BP2_ctr.Y_map = MidRearRecceAreaPt.Y_map - 2500 * Math.Sin(RecceAreaAngle);
            BP2_Adir = RecceAreaAngle;
            BP2_ctr.ConvertMapToScr();

            // обчислення координат точки перетину директрис:
            TPD_Pt.X_map = MidRearRecceAreaPt.X_map + 11000 * Math.Cos(RecceAreaAngle);
            TPD_Pt.Y_map = MidRearRecceAreaPt.Y_map + 11000 * Math.Sin(RecceAreaAngle);
            TPD_Pt.ConvertMapToScr();

            // обчислення координат точки БП-3 та їх переведення в екранну систему координат:
            BP1_ctr.X_map = BP2_ctr.X_map + 4000 * Math.Cos(RecceAreaAngle + Math.PI / 2);
            BP1_ctr.Y_map = BP2_ctr.Y_map + 4000 * Math.Sin(RecceAreaAngle + Math.PI / 2);

            Transforms.OGZ(out double DD1, out BP1_Adir, BP1_ctr.X_map, TPD_Pt.X_map,
                                                         BP1_ctr.Y_map, TPD_Pt.Y_map);
            BP1_ctr.ConvertMapToScr();

            // обчислення координат точки БП-3 та їх переведення в екранну систему координат:
            BP3_ctr.X_map = BP2_ctr.X_map + 4000 * Math.Cos(RecceAreaAngle - Math.PI / 2);
            BP3_ctr.Y_map = BP2_ctr.Y_map + 4000 * Math.Sin(RecceAreaAngle - Math.PI / 2);

            Transforms.OGZ(out double DD3, out BP3_Adir, BP3_ctr.X_map, TPD_Pt.X_map,
                                                         BP3_ctr.Y_map, TPD_Pt.Y_map);
            BP3_ctr.ConvertMapToScr();
            BP_IsDefined = true;
        }

        public void DrawBasnPt(double XscrCTR, double YscrCTR, int R, double Adir, int BP_Nr)
        {

            int X_rectBP = (int)(XscrCTR - R);
            int Y_rectBP = (int)(YscrCTR - R);
            int rectWidth = R * 2;
            int rectHight = R * 2;

            Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            System.Drawing.SolidBrush brush_BP = new System.Drawing.SolidBrush(System.Drawing.Color.Blue);
            System.Drawing.Pen pen_BP = new Pen(System.Drawing.Color.Blue, 2);
            Rectangle rect_BP = new Rectangle(X_rectBP, Y_rectBP, rectWidth, rectHight);

            g.DrawArc(pen_BP, rect_BP, 90 + (float)((Adir + RA) * 180 / Math.PI), 180);

            Font drawFont = new Font("Arial", 9);
            g.DrawString("БП-" + BP_Nr, drawFont, brush_BP, X_rectBP + 2 * R + 1, Y_rectBP + 2 * R + 1);

            // central dash on arc for BP sigh:
            float Xstart_dash1 = (float)(XscrCTR + R * Math.Cos(Math.PI + Adir + RA));
            float Ystart_dash1 = (float)(YscrCTR + R * Math.Sin(Math.PI + Adir + RA));
            float Xend_dash1 = (float)(XscrCTR + 1.33 * R * Math.Cos(Math.PI + Adir + RA));
            float Yend_dash1 = (float)(YscrCTR + 1.33 * R * Math.Sin(Math.PI + Adir + RA));
            g.DrawLine(pen_BP, Xstart_dash1, Ystart_dash1, Xend_dash1, Yend_dash1);

            // left dash on arc for BP sigh:
            float Xstart_dashL = (float)(XscrCTR + R * Math.Cos(Math.PI * 2 / 3 + Adir + RA));
            float Ystart_dashL = (float)(YscrCTR + R * Math.Sin(Math.PI * 2 / 3 + Adir + RA));
            float Xend_dashL = (float)(XscrCTR + 1.33 * R * Math.Cos(Math.PI * 2 / 3 + Adir + RA));
            float Yend_dashL = (float)(YscrCTR + 1.33 * R * Math.Sin(Math.PI * 2 / 3 + Adir + RA));
            g.DrawLine(pen_BP, Xstart_dashL, Ystart_dashL, Xend_dashL, Yend_dashL);

            // right dash on arc for BP sigh:
            float Xstart_dashR = (float)(XscrCTR + R * Math.Cos(Math.PI * 4 / 3 + Adir + RA));
            float Ystart_dashR = (float)(YscrCTR + R * Math.Sin(Math.PI * 4 / 3 + Adir + RA));
            float Xend_dashR = (float)(XscrCTR + 1.33 * R * Math.Cos(Math.PI * 4 / 3 + Adir + RA));
            float Yend_dashR = (float)(YscrCTR + 1.33 * R * Math.Sin(Math.PI * 4 / 3 + Adir + RA));
            g.DrawLine(pen_BP, Xstart_dashR, Ystart_dashR, Xend_dashR, Yend_dashR);

            g.DrawEllipse(pen_BP, (float)XscrCTR, (float)YscrCTR, 1.5f, 1.5f);

            brush_BP.Dispose(); pen_BP.Dispose(); g.Dispose();
        }


        // ******* Drawing of the 1-st Base Point:



        private void BP1_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BP1_ctr.X_scr = RightClickX;
            BP1_ctr.Y_scr = RightClickY;
            BP1_ctr.ConvertScrToMap();
            Transforms.OGZ(out double DD1, out BP1_Adir, BP1_ctr.X_map, TPD_Pt.X_map,
                                                         BP1_ctr.Y_map, TPD_Pt.Y_map);
            DrawBasnPt(BP1_ctr.X_scr, BP1_ctr.Y_scr, BP_radius, BP1_Adir, 1);
            txtAd1.Enabled = true; txtAd1.Text = (BP1_Adir * 30 / Math.PI).ToString(format: "F2"); txtAd1.Enabled = false;
            txtX1.Enabled = true; txtX1.Text = BP1_ctr.X_map.ToString(format: "F0"); txtX1.Enabled = false;
            txtY1.Enabled = true; txtY1.Text = BP1_ctr.Y_map.ToString(format: "F0"); txtY1.Enabled = false;
        }

        private void BP2_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BP2_ctr.X_scr = RightClickX;
            BP2_ctr.Y_scr = RightClickY;
            BP2_ctr.ConvertScrToMap();
            Transforms.OGZ(out double DD2, out BP2_Adir, BP2_ctr.X_map, TPD_Pt.X_map,
                                                         BP2_ctr.Y_map, TPD_Pt.Y_map);
            DrawBasnPt(BP2_ctr.X_scr, BP2_ctr.Y_scr, BP_radius, BP2_Adir, 2);
            txtAd2.Enabled = true; txtAd2.Text = (BP2_Adir * 30 / Math.PI).ToString(format: "F2"); txtAd2.Enabled = false;
            txtX2.Enabled = true; txtX2.Text = BP2_ctr.X_map.ToString(format: "F0"); txtX2.Enabled = false;
            txtY2.Enabled = true; txtY2.Text = BP2_ctr.Y_map.ToString(format: "F0"); txtY2.Enabled = false;
        }

        private void BP3_ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            BP3_ctr.X_scr = RightClickX;
            BP3_ctr.Y_scr = RightClickY;
            BP3_ctr.ConvertScrToMap();
            Transforms.OGZ(out double DD3, out BP3_Adir, BP3_ctr.X_map, TPD_Pt.X_map,
                                                         BP3_ctr.Y_map, TPD_Pt.Y_map);
            DrawBasnPt(BP3_ctr.X_scr, BP3_ctr.Y_scr, BP_radius, BP3_Adir, 3);
            txtAd3.Enabled = true; txtAd3.Text = (BP3_Adir * 30 / Math.PI).ToString(format: "F2"); txtAd3.Enabled = false;
            txtX3.Enabled = true; txtX3.Text = BP3_ctr.X_map.ToString(format: "F0"); txtX3.Enabled = false;
            txtY3.Enabled = true; txtY3.Text = BP3_ctr.Y_map.ToString(format: "F0"); txtY3.Enabled = false;
        }

        private void btnLocateABs_Click(object sender, EventArgs e)
        {
            if (RightRearRecceAreaPt.X_scr != 0 && RightFrontRecceAreaPt.X_scr != 0
                && LeftRearRecceAreaPt.X_scr != 0 && LeftFrontRecceAreaPt.X_scr != 0)
            {
                DefinePBpositions();
                lblNrBP.Text = "Базні пункти:"; lblCPointNr.Visible = false;
                lbl_BP1.Text = "БП-1:"; lbl_BP2.Text = "БП-2:"; lbl_BP3.Text = "БП-3:";

                DrawBasnPt(BP1_ctr.X_scr, BP1_ctr.Y_scr, BP_radius, BP1_Adir, 1);
                txtAd1.Visible = true; lblAdir.Visible = true;
                txtAd1.Text = (BP1_Adir * 30 / Math.PI).ToString(format: "F2"); txtAd1.Enabled = false;
                txtX1.Text = BP1_ctr.X_map.ToString(format: "F0"); txtX1.Enabled = false;
                txtY1.Text = BP1_ctr.Y_map.ToString(format: "F0"); txtY1.Enabled = false;

                DrawBasnPt(BP2_ctr.X_scr, BP2_ctr.Y_scr, BP_radius, BP2_Adir, 2);
                txtAd2.Visible = true;
                txtAd2.Text = (BP2_Adir * 30 / Math.PI).ToString(format: "F2"); txtAd2.Enabled = false;
                txtX2.Text = BP2_ctr.X_map.ToString(format: "F0"); txtX2.Enabled = false;
                txtY2.Text = BP2_ctr.Y_map.ToString(format: "F0"); txtY2.Enabled = false;

                DrawBasnPt(BP3_ctr.X_scr, BP3_ctr.Y_scr, BP_radius, BP3_Adir, 3);
                txtAd3.Visible = true;
                txtAd3.Text = (BP3_Adir * 30 / Math.PI).ToString(format: "F2"); txtAd3.Enabled = false;
                txtX3.Text = BP3_ctr.X_map.ToString(format: "F0"); txtX3.Enabled = false;
                txtY3.Text = BP3_ctr.Y_map.ToString(format: "F0"); txtY3.Enabled = false;
            }
            else MessageBox.Show("Визначте смугу розвідки!");
        }



        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            if (BP_IsDefined)
                pictureBox1.Load(mapfileName);
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            if (BP_IsDefined)
            {
                DrawBasnPt(BP1_ctr.X_scr, BP1_ctr.Y_scr, BP_radius, BP1_Adir, 1);
                DrawBasnPt(BP2_ctr.X_scr, BP2_ctr.Y_scr, BP_radius, BP2_Adir, 2);
                DrawBasnPt(BP3_ctr.X_scr, BP3_ctr.Y_scr, BP_radius, BP3_Adir, 3);
                DrawRecceAreaLines();
                DrawZVR();
            }
        }



        MapPoint[] zvrPtArr = new MapPoint[4]; // 0 - left font, 1 - right frone, 2 right rear, 3 - left rear
        
        private void DrawZVR()
        {
            for (int j = 0; j<4; j++)
            {
                zvrPtArr[j] = new MapPoint(0, 0);
            }
            PointF[] ZvrScrPtArr = new PointF[4];

            Transforms.OGZ(out double GeomBase, out double GeomBaseDir, BP3_ctr.X_map, BP1_ctr.X_map,
                  BP3_ctr.Y_map, BP1_ctr.Y_map); 
            
            GeomBaseDir = GeomBaseDir - (Math.PI / 2); 

            zvrPtArr[0].X_map = BP3_ctr.X_map + 2.54591 * GeomBase * Math.Cos(GeomBaseDir - 0.1974);
            zvrPtArr[0].Y_map = BP3_ctr.Y_map + 2.54591 * GeomBase * Math.Sin(GeomBaseDir - 0.1974);

            zvrPtArr[1].X_map = BP1_ctr.X_map + 2.54591 * GeomBase * Math.Cos(GeomBaseDir + 0.1974);
            zvrPtArr[1].Y_map = BP1_ctr.Y_map + 2.54591 * GeomBase * Math.Sin(GeomBaseDir + 0.1974);

            zvrPtArr[2].X_map = BP1_ctr.X_map + 0.55902 * GeomBase * Math.Cos(GeomBaseDir + 1.10715);
            zvrPtArr[2].Y_map = BP1_ctr.Y_map + 0.55902 * GeomBase * Math.Sin(GeomBaseDir + 1.10715);

            zvrPtArr[3].X_map = BP3_ctr.X_map + 0.55902 * GeomBase * Math.Cos(GeomBaseDir - 1.10715);
            zvrPtArr[3].Y_map = BP3_ctr.Y_map + 0.55902 * GeomBase * Math.Sin(GeomBaseDir - 1.10715);

            for (int i = 0; i<4; i++)
            {
                zvrPtArr[i].ConvertMapToScr();
                ZvrScrPtArr[i].X = Convert.ToSingle(zvrPtArr[i].X_scr);
                ZvrScrPtArr[i].Y = Convert.ToSingle(zvrPtArr[i].Y_scr); // (float)zvrPtArr[i].Y_scr;
            }
           Graphics g = Graphics.FromHwnd(pictureBox1.Handle);
            System.Drawing.SolidBrush brush_ZVRlines = new System.Drawing.SolidBrush(System.Drawing.Color.DarkBlue);
            System.Drawing.Pen pen_ZVRlines = new Pen(brush_ZVRlines);
            pen_ZVRlines.Width = 2;

            // Draw right line:
            if (zvrPtArr[0].X_map != 0 && zvrPtArr[1].X_map != 0)
                g.DrawPolygon(pen_ZVRlines, ZvrScrPtArr);
            brush_ZVRlines.Dispose(); pen_ZVRlines.Dispose(); g.Dispose();
            // .DrawLine(pen_ZVRlines, (float)RightRearRecceAreaPt.X_scr, (float)RightRearRecceAreaPt.Y_scr,
            // (float)RightFrontRecceAreaPt.X_scr, (float)RightFrontRecceAreaPt.Y_scr);
            
        }



        private void button2_Click(object sender, EventArgs e)
        {
            SynchronousSocketClient.StartClient(ConvertDoubleToSendFormat("101", rpt.X_map, rpt.Y_map));
            button2.Text = msg;
        }

        private void BtnSendData_Click(object sender, EventArgs e)
        {
            string Nstring = txtBox_TgtNr.Text;
            string Xstring = txtBox_Xtgt.Text;
            string Ystring = txtBox_Ytgt.Text;
            bool IsConverted = false;
            int Xint = 0;
            int Yint = 0;
            try
            {
                int number = Convert.ToInt32(Nstring, 10);
                Xint = Convert.ToInt32(Xstring, 10);
                Yint = Convert.ToInt32(Ystring, 10);
                IsConverted = true;
            }
            catch (FormatException)
            {
                BtnSendData.Text = "NaN val";
            }

            SrvHostName = textBox_HostName.Text;

            if (IsConverted)
            {
                SynchronousSocketClient.StartClient(ConvertDoubleToSendFormat(Nstring, Xint, Yint));
                BtnSendData.Text = "Sent! " + Nstring;
            }
        }

        string ConvertDoubleToSendFormat(string TgtNr, double X, double Y)
        {
            string result = "";
            char pad = '0';

            string Xstring = (Convert.ToInt32(X)).ToString();
            if (Xstring.Length < 5) Xstring.PadLeft(5, pad);

            string Ystring = (Convert.ToInt32(Y)).ToString();
            if (Ystring.Length < 5) Ystring.PadLeft(5, pad);

            result = TgtNr + "X" + Xstring + "Y" + Ystring + "<EOF>";

            return result;
        }

       
    } // class Form1 : Form
}
