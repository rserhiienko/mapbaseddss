﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
// using System.Math;

namespace TopoMapEngine
{
    class DZU
    {
        public static double GetVirtualTemperature(int Temperature)
        {
            return 0.00003f * Temperature * Temperature * Temperature + 
                   0.0008f * Temperature * Temperature + 
                   0.0208f * Temperature + 
                   0.3232f + 
                   Temperature;
        }

        public static double GetSoundSpeed(double VirtualTemperature, int WindSpeedDZUS_21)
        {
            return WindSpeedDZUS_21 + 0.6 * (VirtualTemperature - 18.3);
        }

        public static double GetPeleng(double SoundSpeed, double BaseLength, int T2, int T3, int dT)
        {
            double sin_peleng = SoundSpeed * (T2 - T3 - dT) / (1000 * BaseLength);
            if ((sin_peleng < 1 ) &&  (sin_peleng > -1 ))
                return (Math.Asin(sin_peleng) * 30 / Math.PI);
            else
                return 0;
        }

        public static double GetWindCorrection(double SoundSpeed, double WindSpeed, double WindAngle, double DirAngle)
        {
            double sin_WindCorr = (WindSpeed / SoundSpeed) * Math.Sin((WindAngle - DirAngle) * Math.PI / 30);
            if ((sin_WindCorr < 0.1) && (sin_WindCorr > -0.1))
                return (Math.Asin(sin_WindCorr) * 30 / Math.PI);
            else
                return 0;
        }

        public static void GetCoord(ref double Xt, ref double Yt, ref double D_difference,             // target coordinates - resection
                                        double X_1, double Y_1, double A_1,   // Base Point (acoustic base) #1 coordinates, target angle in MILS.mils
                                        double X_3, double Y_3, double A_3)   // Base Point (acoustic base) #3 coordinates, target angle in MILS.mils
        { // add more sophisticated A-check  !!!
            if ((A_1 != 15) && (A_1 != 45) && (A_3 != 15) && (A_3 != 45))
            {
            Xt = (X_3 * Math.Tan(A_3 * Math.PI / 30) - X_1 * Math.Tan(A_1 * Math.PI / 30) + Y_1 - Y_3) / 
                 (Math.Tan(A_3 * Math.PI / 30) - Math.Tan(A_1 * Math.PI / 30));
            Yt = (Xt - X_3) * Math.Tan(A_3 * Math.PI / 30) + Y_3;
            }
            
            else if ((A_1 != 0) && (A_1 != 30) && (A_3 != 0) && (A_3 != 30))
            {
                Yt = (Y_1 / Math.Tan(A_1 * Math.PI / 30) - Y_3 / Math.Tan(A_3 * Math.PI / 30) + X_3 - X_1) /
                     (Math.Tan(A_1 * Math.PI / 30) - Math.Tan(A_3 * Math.PI / 30));
                Xt = (Yt - Y_3) / Math.Tan(A_3 * Math.PI / 30) + X_3;
            }
            D_difference = Math.Sqrt((X_1 - Xt) * (X_1 - Xt) + (Y_1 - Yt) * (Y_1 - Yt)) -   //Д_прав - Д_лів
                       Math.Sqrt((X_3 - Xt) * (X_3 - Xt) + (Y_3 - Yt) * (Y_3 - Yt));
        }

        public static void PGZ(ref double Xt, ref double Yt, double X_cab, double Y_cab, double Dt, double At)
        {
            Xt = X_cab + (Dt * Math.Cos(At * Math.PI / 30));
            Yt = Y_cab + (Dt * Math.Sin(At * Math.PI / 30));
        }

        public static void OGZ(double Xa, double Ya, double Xb, double Yb, ref double D, ref double A)
        {
            D = Math.Sqrt((Xb - Xa) * (Xb - Xa) + (Yb - Ya) * (Yb - Ya));
            A = Math.Acos((Xb - Xa) / D) * 30 / Math.PI; // MILS.mils
            if ((Yb - Ya) < 0)
                A = 60 - A;
        }

        public static void GetXYAL(ref double Xcab, ref double Ycab, 
                                   ref double A_dir, ref double L_ab, // (MILS.mils) у великих.малих поділках кутоміра
                                   int Lzp2, int Lzp3, 
                                   double A_zp2, double A_zp3) // mils (в малих поділках кутоміра, оскільки так введено в ДЗУС)
        {
            if ((A_zp2 == 0) && (A_zp3 == 0))
            {
                A_zp2 = A_dir + 15;
                A_zp3 = A_dir - 15;
            }

            double X_zp2 = Xcab + (Lzp2 * Math.Cos(A_zp2 * Math.PI / 30));
            double Y_zp2 = Ycab + (Lzp2 * Math.Sin(A_zp2 * Math.PI / 30));
            double X_zp3 = Xcab + (Lzp3 * Math.Cos(A_zp3 * Math.PI / 30));
            double Y_zp3 = Ycab + (Lzp3 * Math.Sin(A_zp3 * Math.PI / 30));

            Xcab = ((X_zp2 + X_zp3) / 2);
            Ycab = ((Y_zp2 + Y_zp3) / 2);

            L_ab = Math.Sqrt((X_zp3 - X_zp2) * (X_zp3 - X_zp2) + (Y_zp3 - Y_zp2) * (Y_zp3 - Y_zp2));
            A_dir = Math.Acos((X_zp3 - X_zp2) / L_ab) * 30 / Math.PI;
            if ((Y_zp3 - Y_zp2) < 0)
                A_dir = 60 - A_dir;
            A_dir = A_dir + 15;
                          // PGZ(ref X_zp2, ref Y_zp2,   Xcab,   Ycab, Lzp2, A_zp2);

        } // GetXYAL

        public static int GetTau(double L, double Beta, double Cw)      // Peleng, MILS.mils !!!
        {
            double taufloat;
            if (Cw != 0)
                taufloat = ((L / Cw) * Math.Sin(Beta * Math.PI / 30)) * 1000; // miliseconds!!!
            else
                taufloat = 0;
            return (int)Math.Round(taufloat);
        }


        public static double DecodeCodogram(ulong codogram, double Adir, double L_ab, double WindCorr, double WindVelosity, out int T_receive)
        {  //  час * 100 + дир кут цілі в ДУ.ду
            ushort T2, T3, dT, Tastr;
            double peleng, Atgt;
            T2 =    Convert.ToUInt16((codogram & 0x0000FFFF00000000) >> (8*4));
            T3 =    Convert.ToUInt16((codogram & 0xFFFF000000000000) >> (12 * 4));
            dT =    Convert.ToUInt16((codogram & 0x000000000000FF00) >> (2 * 4));
            Tastr = Convert.ToUInt16((codogram & 0x00000000FFFF0000) >> (4 * 4));
            T_receive = Tastr * 1000 - (T2 + T3) / 2000;
            peleng = GetPeleng(WindVelosity, L_ab, T2, T3, dT);
            Atgt = Adir + peleng + WindCorr;
            return Atgt;

        }


    }
}
