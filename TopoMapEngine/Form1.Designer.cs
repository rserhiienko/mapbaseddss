﻿namespace TopoMapEngine
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnMapLoad = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtX1 = new System.Windows.Forms.TextBox();
            this.txtY1 = new System.Windows.Forms.TextBox();
            this.txtX2 = new System.Windows.Forms.TextBox();
            this.txtY2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_BP1 = new System.Windows.Forms.Label();
            this.lbl_BP2 = new System.Windows.Forms.Label();
            this.txtX3 = new System.Windows.Forms.TextBox();
            this.txtY3 = new System.Windows.Forms.TextBox();
            this.lbl_BP3 = new System.Windows.Forms.Label();
            this.btnCalibrate = new System.Windows.Forms.Button();
            this.lblNrBP = new System.Windows.Forms.Label();
            this.lblCPointNr = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnLocateABs = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_HostName = new System.Windows.Forms.TextBox();
            this.lbl_TgtNr = new System.Windows.Forms.Label();
            this.txtBox_TgtNr = new System.Windows.Forms.TextBox();
            this.BtnSendData = new System.Windows.Forms.Button();
            this.lbl_Xtgt = new System.Windows.Forms.Label();
            this.txtBox_Xtgt = new System.Windows.Forms.TextBox();
            this.txtBox_Ytgt = new System.Windows.Forms.TextBox();
            this.lbl_Ytgt = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.калібруватиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.х1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripTextBox2 = new System.Windows.Forms.ToolStripTextBox();
            this.у1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.точка2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.х2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox3 = new System.Windows.Forms.ToolStripTextBox();
            this.у2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox4 = new System.Windows.Forms.ToolStripTextBox();
            this.точка3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.х3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox5 = new System.Windows.Forms.ToolStripTextBox();
            this.у3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox6 = new System.Windows.Forms.ToolStripTextBox();
            this.калібруватиToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.RecceAreaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RightRearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RightFrontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LeftRearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LeftFrontToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DefineAcoustBasesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.RefineLocatiomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BP1_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BP2_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BP3_ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RefineAngleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BP1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.BP2ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.BP3ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.зонаВеденняРозвідкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtAd1 = new System.Windows.Forms.TextBox();
            this.txtAd2 = new System.Windows.Forms.TextBox();
            this.txtAd3 = new System.Windows.Forms.TextBox();
            this.lblAdir = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(16, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(987, 738);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(400, 300);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseClick);
            this.pictureBox1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDoubleClick);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseEnter += new System.EventHandler(this.pictureBox1_MouseEnter);
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // btnMapLoad
            // 
            this.btnMapLoad.Location = new System.Drawing.Point(1041, 17);
            this.btnMapLoad.Margin = new System.Windows.Forms.Padding(4);
            this.btnMapLoad.Name = "btnMapLoad";
            this.btnMapLoad.Size = new System.Drawing.Size(100, 28);
            this.btnMapLoad.TabIndex = 1;
            this.btnMapLoad.Text = "Map Load";
            this.btnMapLoad.UseVisualStyleBackColor = true;
            this.btnMapLoad.Click += new System.EventHandler(this.btnMapLoad_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1165, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1165, 39);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            // 
            // txtX1
            // 
            this.txtX1.Location = new System.Drawing.Point(1073, 118);
            this.txtX1.Margin = new System.Windows.Forms.Padding(4);
            this.txtX1.Name = "txtX1";
            this.txtX1.Size = new System.Drawing.Size(59, 22);
            this.txtX1.TabIndex = 2;
            // 
            // txtY1
            // 
            this.txtY1.Location = new System.Drawing.Point(1140, 118);
            this.txtY1.Margin = new System.Windows.Forms.Padding(4);
            this.txtY1.Name = "txtY1";
            this.txtY1.Size = new System.Drawing.Size(59, 22);
            this.txtY1.TabIndex = 3;
            // 
            // txtX2
            // 
            this.txtX2.Location = new System.Drawing.Point(1073, 150);
            this.txtX2.Margin = new System.Windows.Forms.Padding(4);
            this.txtX2.Name = "txtX2";
            this.txtX2.Size = new System.Drawing.Size(59, 22);
            this.txtX2.TabIndex = 4;
            // 
            // txtY2
            // 
            this.txtY2.Location = new System.Drawing.Point(1140, 150);
            this.txtY2.Margin = new System.Windows.Forms.Padding(4);
            this.txtY2.Name = "txtY2";
            this.txtY2.Size = new System.Drawing.Size(59, 22);
            this.txtY2.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(1092, 95);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Х";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1161, 95);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Y";
            // 
            // lbl_BP1
            // 
            this.lbl_BP1.AutoSize = true;
            this.lbl_BP1.Location = new System.Drawing.Point(1012, 122);
            this.lbl_BP1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_BP1.Name = "lbl_BP1";
            this.lbl_BP1.Size = new System.Drawing.Size(60, 17);
            this.lbl_BP1.TabIndex = 5;
            this.lbl_BP1.Text = "Точка 1";
            // 
            // lbl_BP2
            // 
            this.lbl_BP2.AutoSize = true;
            this.lbl_BP2.Location = new System.Drawing.Point(1012, 154);
            this.lbl_BP2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_BP2.Name = "lbl_BP2";
            this.lbl_BP2.Size = new System.Drawing.Size(60, 17);
            this.lbl_BP2.TabIndex = 5;
            this.lbl_BP2.Text = "Точка 2";
            // 
            // txtX3
            // 
            this.txtX3.Location = new System.Drawing.Point(1073, 182);
            this.txtX3.Margin = new System.Windows.Forms.Padding(4);
            this.txtX3.Name = "txtX3";
            this.txtX3.Size = new System.Drawing.Size(59, 22);
            this.txtX3.TabIndex = 6;
            // 
            // txtY3
            // 
            this.txtY3.Location = new System.Drawing.Point(1140, 182);
            this.txtY3.Margin = new System.Windows.Forms.Padding(4);
            this.txtY3.Name = "txtY3";
            this.txtY3.Size = new System.Drawing.Size(59, 22);
            this.txtY3.TabIndex = 7;
            // 
            // lbl_BP3
            // 
            this.lbl_BP3.AutoSize = true;
            this.lbl_BP3.Location = new System.Drawing.Point(1012, 186);
            this.lbl_BP3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_BP3.Name = "lbl_BP3";
            this.lbl_BP3.Size = new System.Drawing.Size(60, 17);
            this.lbl_BP3.TabIndex = 5;
            this.lbl_BP3.Text = "Точка 3";
            // 
            // btnCalibrate
            // 
            this.btnCalibrate.Location = new System.Drawing.Point(1061, 215);
            this.btnCalibrate.Margin = new System.Windows.Forms.Padding(4);
            this.btnCalibrate.Name = "btnCalibrate";
            this.btnCalibrate.Size = new System.Drawing.Size(155, 28);
            this.btnCalibrate.TabIndex = 8;
            this.btnCalibrate.Text = "Калібрувати карту";
            this.btnCalibrate.UseVisualStyleBackColor = true;
            this.btnCalibrate.Click += new System.EventHandler(this.btnCalibrate_Click);
            // 
            // lblNrBP
            // 
            this.lblNrBP.AutoSize = true;
            this.lblNrBP.Location = new System.Drawing.Point(1037, 73);
            this.lblNrBP.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNrBP.Name = "lblNrBP";
            this.lblNrBP.Size = new System.Drawing.Size(169, 17);
            this.lblNrBP.TabIndex = 7;
            this.lblNrBP.Text = "Н-р точки калібрування:\r\n";
            // 
            // lblCPointNr
            // 
            this.lblCPointNr.AutoSize = true;
            this.lblCPointNr.Location = new System.Drawing.Point(1213, 73);
            this.lblCPointNr.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCPointNr.Name = "lblCPointNr";
            this.lblCPointNr.Size = new System.Drawing.Size(16, 17);
            this.lblCPointNr.TabIndex = 8;
            this.lblCPointNr.Text = "#";
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 553);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(1261, 25);
            this.statusStrip1.TabIndex = 9;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(156, 20);
            this.toolStripStatusLabel1.Text = "Програму запущено!";
            // 
            // btnLocateABs
            // 
            this.btnLocateABs.Location = new System.Drawing.Point(1016, 260);
            this.btnLocateABs.Margin = new System.Windows.Forms.Padding(4);
            this.btnLocateABs.Name = "btnLocateABs";
            this.btnLocateABs.Size = new System.Drawing.Size(227, 43);
            this.btnLocateABs.TabIndex = 11;
            this.btnLocateABs.Text = "Визначити положення акустичних баз";
            this.btnLocateABs.UseVisualStyleBackColor = true;
            this.btnLocateABs.Click += new System.EventHandler(this.btnLocateABs_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.textBox_HostName);
            this.groupBox2.Controls.Add(this.lbl_TgtNr);
            this.groupBox2.Controls.Add(this.txtBox_TgtNr);
            this.groupBox2.Controls.Add(this.BtnSendData);
            this.groupBox2.Controls.Add(this.lbl_Xtgt);
            this.groupBox2.Controls.Add(this.txtBox_Xtgt);
            this.groupBox2.Controls.Add(this.txtBox_Ytgt);
            this.groupBox2.Controls.Add(this.lbl_Ytgt);
            this.groupBox2.Location = new System.Drawing.Point(1018, 311);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox2.Size = new System.Drawing.Size(225, 141);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Input data to send";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(33, 17);
            this.label5.TabIndex = 18;
            this.label5.Text = "Srv:";
            // 
            // textBox_HostName
            // 
            this.textBox_HostName.Location = new System.Drawing.Point(78, 77);
            this.textBox_HostName.Name = "textBox_HostName";
            this.textBox_HostName.Size = new System.Drawing.Size(140, 22);
            this.textBox_HostName.TabIndex = 17;
            this.textBox_HostName.Text = "WIN-2JF1S2VBSQ5";
            // 
            // lbl_TgtNr
            // 
            this.lbl_TgtNr.AutoSize = true;
            this.lbl_TgtNr.Location = new System.Drawing.Point(34, 22);
            this.lbl_TgtNr.Name = "lbl_TgtNr";
            this.lbl_TgtNr.Size = new System.Drawing.Size(23, 17);
            this.lbl_TgtNr.TabIndex = 16;
            this.lbl_TgtNr.Text = "Nr";
            // 
            // txtBox_TgtNr
            // 
            this.txtBox_TgtNr.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_TgtNr.Location = new System.Drawing.Point(22, 42);
            this.txtBox_TgtNr.MaxLength = 3;
            this.txtBox_TgtNr.Name = "txtBox_TgtNr";
            this.txtBox_TgtNr.Size = new System.Drawing.Size(48, 24);
            this.txtBox_TgtNr.TabIndex = 15;
            // 
            // BtnSendData
            // 
            this.BtnSendData.Location = new System.Drawing.Point(22, 111);
            this.BtnSendData.Name = "BtnSendData";
            this.BtnSendData.Size = new System.Drawing.Size(168, 23);
            this.BtnSendData.TabIndex = 14;
            this.BtnSendData.Text = "SendData";
            this.BtnSendData.UseVisualStyleBackColor = true;
            this.BtnSendData.Click += new System.EventHandler(this.BtnSendData_Click);
            // 
            // lbl_Xtgt
            // 
            this.lbl_Xtgt.AutoSize = true;
            this.lbl_Xtgt.Location = new System.Drawing.Point(104, 22);
            this.lbl_Xtgt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Xtgt.Name = "lbl_Xtgt";
            this.lbl_Xtgt.Size = new System.Drawing.Size(17, 17);
            this.lbl_Xtgt.TabIndex = 5;
            this.lbl_Xtgt.Text = "Х";
            // 
            // txtBox_Xtgt
            // 
            this.txtBox_Xtgt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Xtgt.Location = new System.Drawing.Point(78, 42);
            this.txtBox_Xtgt.Margin = new System.Windows.Forms.Padding(4);
            this.txtBox_Xtgt.MaxLength = 5;
            this.txtBox_Xtgt.Name = "txtBox_Xtgt";
            this.txtBox_Xtgt.Size = new System.Drawing.Size(67, 24);
            this.txtBox_Xtgt.TabIndex = 2;
            // 
            // txtBox_Ytgt
            // 
            this.txtBox_Ytgt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBox_Ytgt.Location = new System.Drawing.Point(153, 42);
            this.txtBox_Ytgt.Margin = new System.Windows.Forms.Padding(4);
            this.txtBox_Ytgt.MaxLength = 5;
            this.txtBox_Ytgt.Name = "txtBox_Ytgt";
            this.txtBox_Ytgt.Size = new System.Drawing.Size(66, 24);
            this.txtBox_Ytgt.TabIndex = 3;
            // 
            // lbl_Ytgt
            // 
            this.lbl_Ytgt.AutoSize = true;
            this.lbl_Ytgt.Location = new System.Drawing.Point(175, 22);
            this.lbl_Ytgt.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl_Ytgt.Name = "lbl_Ytgt";
            this.lbl_Ytgt.Size = new System.Drawing.Size(17, 17);
            this.lbl_Ytgt.TabIndex = 5;
            this.lbl_Ytgt.Text = "Y";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.калібруватиToolStripMenuItem,
            this.точка2ToolStripMenuItem,
            this.точка3ToolStripMenuItem,
            this.калібруватиToolStripMenuItem1,
            this.toolStripSeparator1,
            this.RecceAreaToolStripMenuItem,
            this.DefineAcoustBasesToolStripMenuItem,
            this.toolStripSeparator2,
            this.RefineLocatiomToolStripMenuItem,
            this.RefineAngleToolStripMenuItem,
            this.toolStripSeparator3,
            this.зонаВеденняРозвідкиToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(240, 238);
            // 
            // калібруватиToolStripMenuItem
            // 
            this.калібруватиToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.х1ToolStripMenuItem,
            this.у1ToolStripMenuItem});
            this.калібруватиToolStripMenuItem.Name = "калібруватиToolStripMenuItem";
            this.калібруватиToolStripMenuItem.Size = new System.Drawing.Size(239, 24);
            this.калібруватиToolStripMenuItem.Text = "Точка прив\'язки №1";
            // 
            // х1ToolStripMenuItem
            // 
            this.х1ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1,
            this.toolStripTextBox2});
            this.х1ToolStripMenuItem.Name = "х1ToolStripMenuItem";
            this.х1ToolStripMenuItem.Size = new System.Drawing.Size(111, 26);
            this.х1ToolStripMenuItem.Text = "Х1=";
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 27);
            // 
            // toolStripTextBox2
            // 
            this.toolStripTextBox2.Name = "toolStripTextBox2";
            this.toolStripTextBox2.Size = new System.Drawing.Size(100, 27);
            // 
            // у1ToolStripMenuItem
            // 
            this.у1ToolStripMenuItem.Name = "у1ToolStripMenuItem";
            this.у1ToolStripMenuItem.Size = new System.Drawing.Size(111, 26);
            this.у1ToolStripMenuItem.Text = "У1=";
            // 
            // точка2ToolStripMenuItem
            // 
            this.точка2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.х2ToolStripMenuItem,
            this.у2ToolStripMenuItem});
            this.точка2ToolStripMenuItem.Name = "точка2ToolStripMenuItem";
            this.точка2ToolStripMenuItem.Size = new System.Drawing.Size(239, 24);
            this.точка2ToolStripMenuItem.Text = "Точка прив\'язки №2";
            // 
            // х2ToolStripMenuItem
            // 
            this.х2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox3});
            this.х2ToolStripMenuItem.Name = "х2ToolStripMenuItem";
            this.х2ToolStripMenuItem.Size = new System.Drawing.Size(111, 26);
            this.х2ToolStripMenuItem.Text = "Х2=";
            // 
            // toolStripTextBox3
            // 
            this.toolStripTextBox3.Name = "toolStripTextBox3";
            this.toolStripTextBox3.Size = new System.Drawing.Size(100, 27);
            // 
            // у2ToolStripMenuItem
            // 
            this.у2ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox4});
            this.у2ToolStripMenuItem.Name = "у2ToolStripMenuItem";
            this.у2ToolStripMenuItem.Size = new System.Drawing.Size(111, 26);
            this.у2ToolStripMenuItem.Text = "У2=";
            // 
            // toolStripTextBox4
            // 
            this.toolStripTextBox4.Name = "toolStripTextBox4";
            this.toolStripTextBox4.Size = new System.Drawing.Size(100, 27);
            // 
            // точка3ToolStripMenuItem
            // 
            this.точка3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.х3ToolStripMenuItem,
            this.у3ToolStripMenuItem});
            this.точка3ToolStripMenuItem.Name = "точка3ToolStripMenuItem";
            this.точка3ToolStripMenuItem.Size = new System.Drawing.Size(239, 24);
            this.точка3ToolStripMenuItem.Text = "Точка прив\'язки №3";
            // 
            // х3ToolStripMenuItem
            // 
            this.х3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox5});
            this.х3ToolStripMenuItem.Name = "х3ToolStripMenuItem";
            this.х3ToolStripMenuItem.Size = new System.Drawing.Size(111, 26);
            this.х3ToolStripMenuItem.Text = "Х3=";
            // 
            // toolStripTextBox5
            // 
            this.toolStripTextBox5.Name = "toolStripTextBox5";
            this.toolStripTextBox5.Size = new System.Drawing.Size(100, 27);
            // 
            // у3ToolStripMenuItem
            // 
            this.у3ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox6});
            this.у3ToolStripMenuItem.Name = "у3ToolStripMenuItem";
            this.у3ToolStripMenuItem.Size = new System.Drawing.Size(111, 26);
            this.у3ToolStripMenuItem.Text = "У3=";
            // 
            // toolStripTextBox6
            // 
            this.toolStripTextBox6.Name = "toolStripTextBox6";
            this.toolStripTextBox6.Size = new System.Drawing.Size(100, 27);
            // 
            // калібруватиToolStripMenuItem1
            // 
            this.калібруватиToolStripMenuItem1.Name = "калібруватиToolStripMenuItem1";
            this.калібруватиToolStripMenuItem1.Size = new System.Drawing.Size(239, 24);
            this.калібруватиToolStripMenuItem1.Text = "Калібрувати";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(236, 6);
            // 
            // RecceAreaToolStripMenuItem
            // 
            this.RecceAreaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.RightRearToolStripMenuItem,
            this.RightFrontToolStripMenuItem,
            this.LeftRearToolStripMenuItem,
            this.LeftFrontToolStripMenuItem});
            this.RecceAreaToolStripMenuItem.Name = "RecceAreaToolStripMenuItem";
            this.RecceAreaToolStripMenuItem.Size = new System.Drawing.Size(239, 24);
            this.RecceAreaToolStripMenuItem.Text = "Смуга розвідки:";
            // 
            // RightRearToolStripMenuItem
            // 
            this.RightRearToolStripMenuItem.Name = "RightRearToolStripMenuItem";
            this.RightRearToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.RightRearToolStripMenuItem.Text = "Права ближня";
            this.RightRearToolStripMenuItem.Click += new System.EventHandler(this.RightRearToolStripMenuItem_Click);
            // 
            // RightFrontToolStripMenuItem
            // 
            this.RightFrontToolStripMenuItem.Name = "RightFrontToolStripMenuItem";
            this.RightFrontToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.RightFrontToolStripMenuItem.Text = "Права дальня";
            this.RightFrontToolStripMenuItem.Click += new System.EventHandler(this.RightFrontToolStripMenuItem_Click);
            // 
            // LeftRearToolStripMenuItem
            // 
            this.LeftRearToolStripMenuItem.Name = "LeftRearToolStripMenuItem";
            this.LeftRearToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.LeftRearToolStripMenuItem.Text = "Ліва ближня";
            this.LeftRearToolStripMenuItem.Click += new System.EventHandler(this.LeftRearToolStripMenuItem_Click);
            // 
            // LeftFrontToolStripMenuItem
            // 
            this.LeftFrontToolStripMenuItem.Name = "LeftFrontToolStripMenuItem";
            this.LeftFrontToolStripMenuItem.Size = new System.Drawing.Size(186, 26);
            this.LeftFrontToolStripMenuItem.Text = "Ліва дальня";
            this.LeftFrontToolStripMenuItem.Click += new System.EventHandler(this.LeftFrontToolStripMenuItem_Click);
            // 
            // DefineAcoustBasesToolStripMenuItem
            // 
            this.DefineAcoustBasesToolStripMenuItem.Name = "DefineAcoustBasesToolStripMenuItem";
            this.DefineAcoustBasesToolStripMenuItem.Size = new System.Drawing.Size(239, 24);
            this.DefineAcoustBasesToolStripMenuItem.Text = "Визначити ак.бази";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(236, 6);
            // 
            // RefineLocatiomToolStripMenuItem
            // 
            this.RefineLocatiomToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BP1_ToolStripMenuItem,
            this.BP2_ToolStripMenuItem,
            this.BP3_ToolStripMenuItem});
            this.RefineLocatiomToolStripMenuItem.Name = "RefineLocatiomToolStripMenuItem";
            this.RefineLocatiomToolStripMenuItem.Size = new System.Drawing.Size(239, 24);
            this.RefineLocatiomToolStripMenuItem.Text = "Уточнити місце...";
            // 
            // BP1_ToolStripMenuItem
            // 
            this.BP1_ToolStripMenuItem.Name = "BP1_ToolStripMenuItem";
            this.BP1_ToolStripMenuItem.Size = new System.Drawing.Size(118, 26);
            this.BP1_ToolStripMenuItem.Text = "БП-1";
            this.BP1_ToolStripMenuItem.Click += new System.EventHandler(this.BP1_ToolStripMenuItem_Click);
            // 
            // BP2_ToolStripMenuItem
            // 
            this.BP2_ToolStripMenuItem.Name = "BP2_ToolStripMenuItem";
            this.BP2_ToolStripMenuItem.Size = new System.Drawing.Size(118, 26);
            this.BP2_ToolStripMenuItem.Text = "БП-2";
            this.BP2_ToolStripMenuItem.Click += new System.EventHandler(this.BP2_ToolStripMenuItem_Click);
            // 
            // BP3_ToolStripMenuItem
            // 
            this.BP3_ToolStripMenuItem.Name = "BP3_ToolStripMenuItem";
            this.BP3_ToolStripMenuItem.Size = new System.Drawing.Size(118, 26);
            this.BP3_ToolStripMenuItem.Text = "БП-3";
            this.BP3_ToolStripMenuItem.Click += new System.EventHandler(this.BP3_ToolStripMenuItem_Click);
            // 
            // RefineAngleToolStripMenuItem
            // 
            this.RefineAngleToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BP1ToolStripMenuItem1,
            this.BP2ToolStripMenuItem1,
            this.BP3ToolStripMenuItem1});
            this.RefineAngleToolStripMenuItem.Name = "RefineAngleToolStripMenuItem";
            this.RefineAngleToolStripMenuItem.Size = new System.Drawing.Size(239, 24);
            this.RefineAngleToolStripMenuItem.Text = "Уточнити директрису...";
            // 
            // BP1ToolStripMenuItem1
            // 
            this.BP1ToolStripMenuItem1.Name = "BP1ToolStripMenuItem1";
            this.BP1ToolStripMenuItem1.Size = new System.Drawing.Size(118, 26);
            this.BP1ToolStripMenuItem1.Text = "БП-1";
            // 
            // BP2ToolStripMenuItem1
            // 
            this.BP2ToolStripMenuItem1.Name = "BP2ToolStripMenuItem1";
            this.BP2ToolStripMenuItem1.Size = new System.Drawing.Size(118, 26);
            this.BP2ToolStripMenuItem1.Text = "БП-2";
            // 
            // BP3ToolStripMenuItem1
            // 
            this.BP3ToolStripMenuItem1.Name = "BP3ToolStripMenuItem1";
            this.BP3ToolStripMenuItem1.Size = new System.Drawing.Size(118, 26);
            this.BP3ToolStripMenuItem1.Text = "БП-3";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(236, 6);
            // 
            // зонаВеденняРозвідкиToolStripMenuItem
            // 
            this.зонаВеденняРозвідкиToolStripMenuItem.Name = "зонаВеденняРозвідкиToolStripMenuItem";
            this.зонаВеденняРозвідкиToolStripMenuItem.Size = new System.Drawing.Size(239, 24);
            this.зонаВеденняРозвідкиToolStripMenuItem.Text = "Зона ведення розвідки";
            // 
            // txtAd1
            // 
            this.txtAd1.Location = new System.Drawing.Point(1205, 118);
            this.txtAd1.Margin = new System.Windows.Forms.Padding(4);
            this.txtAd1.Name = "txtAd1";
            this.txtAd1.Size = new System.Drawing.Size(43, 22);
            this.txtAd1.TabIndex = 3;
            this.txtAd1.Visible = false;
            // 
            // txtAd2
            // 
            this.txtAd2.Location = new System.Drawing.Point(1205, 150);
            this.txtAd2.Margin = new System.Windows.Forms.Padding(4);
            this.txtAd2.Name = "txtAd2";
            this.txtAd2.Size = new System.Drawing.Size(43, 22);
            this.txtAd2.TabIndex = 3;
            this.txtAd2.Visible = false;
            // 
            // txtAd3
            // 
            this.txtAd3.Location = new System.Drawing.Point(1205, 182);
            this.txtAd3.Margin = new System.Windows.Forms.Padding(4);
            this.txtAd3.Name = "txtAd3";
            this.txtAd3.Size = new System.Drawing.Size(43, 22);
            this.txtAd3.TabIndex = 3;
            this.txtAd3.Visible = false;
            // 
            // lblAdir
            // 
            this.lblAdir.AutoSize = true;
            this.lblAdir.Location = new System.Drawing.Point(1205, 95);
            this.lblAdir.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblAdir.Name = "lblAdir";
            this.lblAdir.Size = new System.Drawing.Size(41, 17);
            this.lblAdir.TabIndex = 5;
            this.lblAdir.Text = "Адир";
            this.lblAdir.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Location = new System.Drawing.Point(1029, 477);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 65);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Idle controls";
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Location = new System.Drawing.Point(14, 23);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(163, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "Start_connection";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1261, 578);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnLocateABs);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.lblCPointNr);
            this.Controls.Add(this.lblNrBP);
            this.Controls.Add(this.btnCalibrate);
            this.Controls.Add(this.lblAdir);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbl_BP3);
            this.Controls.Add(this.lbl_BP2);
            this.Controls.Add(this.lbl_BP1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtY3);
            this.Controls.Add(this.txtY2);
            this.Controls.Add(this.txtX3);
            this.Controls.Add(this.txtX2);
            this.Controls.Add(this.txtAd3);
            this.Controls.Add(this.txtAd2);
            this.Controls.Add(this.txtAd1);
            this.Controls.Add(this.txtY1);
            this.Controls.Add(this.txtX1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMapLoad);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Планування розгортання звукометричного комплексу АЗК-7";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnMapLoad;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtX1;
        private System.Windows.Forms.TextBox txtY1;
        private System.Windows.Forms.TextBox txtX2;
        private System.Windows.Forms.TextBox txtY2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_BP1;
        private System.Windows.Forms.Label lbl_BP2;
        private System.Windows.Forms.TextBox txtX3;
        private System.Windows.Forms.TextBox txtY3;
        private System.Windows.Forms.Label lbl_BP3;
        private System.Windows.Forms.Button btnCalibrate;
        private System.Windows.Forms.Label lblNrBP;
        private System.Windows.Forms.Label lblCPointNr;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnLocateABs;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem калібруватиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem х1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox2;
        private System.Windows.Forms.ToolStripMenuItem у1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem точка2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem х2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox3;
        private System.Windows.Forms.ToolStripMenuItem у2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox4;
        private System.Windows.Forms.ToolStripMenuItem точка3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem х3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox5;
        private System.Windows.Forms.ToolStripMenuItem у3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox6;
        private System.Windows.Forms.ToolStripMenuItem калібруватиToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem RecceAreaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RightRearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RightFrontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LeftRearToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LeftFrontToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DefineAcoustBasesToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem RefineLocatiomToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BP1_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BP2_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BP3_ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RefineAngleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BP1ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem BP2ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem BP3ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem зонаВеденняРозвідкиToolStripMenuItem;
        private System.Windows.Forms.TextBox txtAd1;
        private System.Windows.Forms.TextBox txtAd2;
        private System.Windows.Forms.TextBox txtAd3;
        private System.Windows.Forms.Label lblAdir;
        private System.Windows.Forms.Button BtnSendData;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label lbl_Xtgt;
        private System.Windows.Forms.TextBox txtBox_Xtgt;
        private System.Windows.Forms.TextBox txtBox_Ytgt;
        private System.Windows.Forms.Label lbl_Ytgt;
        private System.Windows.Forms.Label lbl_TgtNr;
        private System.Windows.Forms.TextBox txtBox_TgtNr;
        public System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_HostName;
    }
}

